/**
 *  PhpStorm
 *
 *  Created by: Valery Ivannikov
 *  Site: http://varelkka.com
 */

$(document).ready(function () {
    var $win = $(window), windowWidth = $win.width(), dontSlick = false, $doc = $(document),
        firstOpen = true  ; // True - линии рисуются с анимацией
    var mtop = [], mleft = [], mwidth = [], mheight = [], Lh = 7; // Используются для рисования линий


    $(".inp-phone").mask("+7 (999) 999-99-99");

    $(".inp-confirm").mask("9 9 9 9");


    if (windowWidth < 600) {
        $('.frm-inp.frm-date').each(function () {
            var $width = $(this).parent().width(),
                $widthButt = $(this).parent().find('.a-fore-date').width();
            $(this).css('width', ($width - $widthButt - 4) + 'px');
        })

        $('#city').css('width', '130px');
    };

    $('.b-frmpop').each(function () {
        $(this).css({'visibility': 'visible', 'display': 'none'})
    });

    $('[data-lazy-bg]').each(function () {
        var img = $(this).data('lazy-bg');
        $(this).css({'background-image': 'url(' + img + ')'})
    });
    $('[data-lazy-im]').each(function () {
        var img = $(this).data('lazy-bg');
        $(this).attr('src', img);
    });


    $win.scroll(function () {
        var doc = $doc.scrollTop();


        if (doc > 10) {
            $('#headerID,#l1,#l2').addClass('fxt');
        } else {
            $('#headerID,#l1,#l2').removeClass('fxt');
        }

        if (doc == 0) {
            if (windowWidth > 800) {
                $('#menutopID').fadeIn();
            }
        }
    });


    $('.js-top,.a-logo,.js-scroll').on('click', function () {
        var id = $(this).attr('href').replace('/', ''),
            top;
        top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
        return false;
    })


    $('#menutopID a').on('click', function () {
        var id = $(this).attr('href').replace('/', ''),
            top;
        top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
        if (windowWidth < 800) {
            $('#menutopID').fadeOut();
        }
        if ($('#mobMenu').hasClass('open')) {

            $('#mobMenu').removeClass('open');
            $('#headerID').removeClass('open');
        }

        return false;
    });


    $('body').click(function (e) {
        if (e.target.id != 'search')
            $('.simplesearch-search-form.active').removeClass('active');

    });

    $('#mobMenu').on('click', function () {
        if (windowWidth < 800) {
            $('#menutopID').fadeToggle();
            $('#mobMenu').toggleClass('open');
            $('#headerID').toggleClass('open');
        }
        return false;
    });


    $(window).resize(function () {
        windowWidth = $win.width();
        $('.line').css('display', 'none');
    });

    function debounce(func) {
        var timer;
        return function (event) {
            if (timer) clearTimeout(timer);
            timer = setTimeout(func, 100, event);
        };
    }

    window.addEventListener("resize", debounce(function (e) {
        setTimeout(lineDraw, 1000);
    }));

    document._video = document.getElementById("bVideoID");

    function getVideo() {
        return document._video;
    }

    $(document).on('click', '.a-play', function () {
        $(this).fadeOut();
        $('.b-video img').css({'opacity': 0, 'z-index': -2});
        $('#bVideoID').css({'opacity': 1, 'z-index': 2});
        getVideo().play();

        return false;

    });
//=include inc/sliders.js
//=include inc/popup.js
//=include inc/line.js
//=include inc/map.js
});