/************************************************************
 *  LINES DRAW - START
 ************************************************************/

(function () {
    var linesArr = ["line h", "line v", "line h", "line v", "line h", "line v", "line h", "line v", "line h", "line h", "line v", "line v", "line h", "line h", "line v", "line h", "line h y", "line v y", "line v y", "line h y", "line v y", "line h y", "line v y", "line h y", "line h y", "line v y", "line h y", "line v y", "line h y", "line h y", "line v y", "line h y", "line v y", "line h", "line v", "line h", "line v", "line v", "line h", "line v", "line h", "line v", "line h", "line v", "line v", "line h", "line v", "line h"],
        $body = $('#bPage'),
        id = 1;
    linesArr.forEach(function (entry) {
        $body.prepend('<div id="l' + id + '" class="' + entry + '"></div>');
        id++;
    })
})();


function lineDraw() {
    var center = windowWidth / 2, pos, pos1, width, height, top = 90;
    if (windowWidth < 800) {
        top = 50;
    }

    $('.line').fadeIn();
    if (!firstOpen) {
        $('#l1,#l2,#l3,#l4').addClass('no-transition');

        $('#l1').css({
            'right': center + ((windowWidth * 10) / 100),
            'width': ((windowWidth * 10.76) / 100),
            'top': top
        });

        pos = $('#l1').offset();
        $('#l2').css({'left': pos.left, 'height': ((windowWidth * 22.5) / 100), 'top': pos.top});

        height = $('#l2').height();
        pos = $('#l2').offset();
        $('#l3').css({'left': pos.left, 'width': ((windowWidth * 67) / 100), 'top': top + height});


        mtop[4] = pos = $('#l3').offset();
        width = $('#l3').width();
        pos1 = $('#for_line4').offset();
        mleft[4] = pos.left + width;
        mheight[4] = pos1.top - pos.top + ((windowWidth * 10) / 100)
        if (windowWidth < 600) {
            mleft[4] = windowWidth / 2;
            mtop[4].top = 300;
            pos8 = $('#for_line_4').offset();
            height8 = $('#for_line_4').height();
            mheight[4] = pos8.top - mtop[4].top + height8 - 50;
        }
        $('#l4').css({'left': mleft[4], 'height': mheight[4], 'top': mtop[4].top});


        lineDrawOver(mleft[4], width, false)

    } else {
        $('#l1').css({
            'right': center + ((windowWidth * 10) / 100),
            'width': ((windowWidth * 10.76) / 100),
            'top': top
        });
        element = document.getElementById("l1");
        element.addEventListener("transitionend", function () {
            pos = $('#l1').offset();
            $('#l2').css({'left': pos.left, 'height': ((windowWidth * 22.5) / 100), 'top': pos.top});
        }, false);

        element2 = document.getElementById("l2");
        element2.addEventListener("transitionend", function () {
            height = $('#l2').height();
            pos = $('#l2').offset();
            $('#l3').css({'left': pos.left, 'width': ((windowWidth * 67) / 100), 'top': top + height});
        }, false);

        element3 = document.getElementById("l3");
        element3.addEventListener("transitionend", function () {
            mtop[4] = pos = $('#l3').offset();
            width = $('#l3').width();
            pos1 = $('#for_line4').offset();
            mleft[4] = pos.left + width;
            mheight[4] = pos1.top - pos.top + ((windowWidth * 10) / 100)
            if (windowWidth < 600) {
                mleft[4] = windowWidth / 2;
                mtop[4].top = 300;
                pos8 = $('#for_line_4').offset();
                height8 = $('#for_line_4').height();
                mheight[4] = pos8.top - mtop[4].top + height8 - 50;
            }
            $('#l4').css({'left': mleft[4], 'height': mheight[4], 'top': mtop[4].top});
        }, false);

        element4 = document.getElementById("l4");
        element4.addEventListener("transitionend", function () {
                lineDrawOver(mleft[4], width, false)
            }
            , false);

    }
    firstOpen = false;
}


function lineDrawOver(pos, width, isredraw) {

    if (windowWidth < 1400) {
        Lh = 5;
    }
    if (windowWidth < 1100) {
        Lh = 4;
    }
    if (windowWidth < 800) {
        Lh = 3;
    }

    if (!isredraw) {

        pos = $('#l4').offset();
        height = $('#l4').height();
        pos1 = ((windowWidth * 4.5) / 100);
        mleft[5] = pos1;
        mtop[5] = pos.top + height;
        mwidth[5] = pos.left - pos1 + Lh;
        if (windowWidth < 800 && windowWidth > 600) {
            pos1 = windowWidth * 0.02;
            mleft[5] = pos1;
            mwidth[5] = pos.left - pos1 + Lh;
        }

        if (windowWidth < 600) {
            mleft[5] = mleft[4];
            mwidth[5] = windowWidth - mleft[5] - ((windowWidth * 10) / 100);
        }
        $('#l5').css({'left': mleft[5], 'width': mwidth[5], 'top': mtop[5]});


        pos = $('#l5').offset();
        pos1 = $('#for_line6').offset()
        height4 = $('#for_line6').height()
        mheight[6] = pos1.top + height4 - pos.top;
        mleft[6] = pos.left

        if (windowWidth < 600) {
            mleft[6] = mleft[5] + mwidth[5];
            poss = $('#for_line4').offset();
            mheight[6] = poss.top - pos.top + 16;
        }


        $('#l6').css({'left': mleft[6], 'height': mheight[6], 'top': pos.top});
        pos = $('#l6').offset();
        pos111 = $('#s1_bottom').offset();
        pos222 = $('#l4').offset();
        if (windowWidth < 600) {
            mleft[45] = mleft[6];
        } else {
            mleft[45] = pos222.left;
        }
        $('#l45').css({'left': mleft[45], 'height': 60, 'top': pos111.top - 2});

        height = $('#l6').height();
        mwidth[7] = ((windowWidth * 15.5) / 100);
        if (windowWidth < 600) {
            mwidth[7] = windowWidth - mleft[6];
        }

        $('#l7').css({'left': pos.left, 'width': mwidth[7], 'top': pos.top + height});
    }
    width = $('#l7').width();
    pos1 = $('#l7').offset();
    pos2 = $('#for_line8').offset();
    if (windowWidth < 1400) {
        pos2.top = pos2.top + ((windowWidth * 5) / 100)
    }
    mheight[8] = pos2.top - pos1.top;
    $height8 = mheight[8];
    $('#l8').css({'left': pos1.left + width, 'height': mheight[8], 'top': pos.top + height});
    height = $('#l8').height() + height;
    $('#l9').css({'left': pos1.left + width, 'width': ((windowWidth * 18) / 100), 'top': pos.top + height});
    pos2 = $('#l4').offset();

    mwidth[10] = width = ((windowWidth * 18) / 100);
    top1 = pos2.top + ((windowWidth * 5) / 100);

    mtop[10] = pos.top + height + ((windowWidth * 10) / 100)
    mleft[10] = pos2.left - mwidth[10];
    if (windowWidth < 1100) {
        mtop[10] = pos.top + height + ((windowWidth * 20) / 100)
        top1 = pos2.top + ((windowWidth * 15) / 100);
        mwidth[10] = width = ((windowWidth * 15) / 100);
        mleft[10] = pos2.left - mwidth[10];

    }
    if (windowWidth < 600) {
        mtop[10] = pos.top + height + ((windowWidth * 10) / 100)
        top1 = pos2.top + ((windowWidth * 10) / 100);
        mwidth[10] = width = ((windowWidth * 30) / 100);
        mleft[10] = 0;
    }


    $('#l10').css({'left': mleft[10], 'width': mwidth[10], 'top': mtop[10]});
    pos3 = $('#slider3').offset();
    height33 = $('#slider3').height();
    pos1 = $('#l10').offset();
    mleft[11] = pos2.left;

    if (windowWidth < 600) {
        mleft[11] = windowWidth * 0.75;
    }
    mheight[11] = pos3.top - pos1.top + height33 * 0.7;
    mtop[11] = mtop[10];

    $('#l11').css({'left': mleft[11], 'height': mheight[11], 'top': mtop[10]});


    pos1 = $('#slider3 .slider3_item.slick-active').offset();
    height = $('#slider3 .slider3_item.slick-active').height();


    mtop[12] = mtop[11] + mheight[11];
    pos2 = $('#for_line12').offset();
    height = $('#for_line12').height();
    mheight[12] = pos2.top - mtop[12] + height / 2 - 50;
    mleft[12] = pos1.left + 40;

    if (windowWidth < 600) {
        mleft[12] = windowWidth * 0.96;
        mheight[12] = pos2.top - mtop[12] + height + 100;
    }

    $('#l12').css({'left': mleft[12], 'height': mheight[12], 'top': mtop[12]});


    mtop[48] = mtop[11] + mheight[11];
    mwidth[48] = mleft[11] - mleft[12];
    mleft[48] = mleft[11] - mwidth[48] + Lh;

    $('#l48').css({'left': mleft[48], width: mwidth[48], 'top': mtop[48]});

    mtop[13] = mtop[12] + mheight[12];
    mleft[13] = mleft[12];
    mwidth[13] = (pos2.left - ((windowWidth * 14) / 100)) - mleft[13];

    if (windowWidth < 600) {
        mwidth[13] = windowWidth / 2 - (windowWidth - mleft[12] );
        mleft[13] = mleft[12] - mwidth[13] + Lh;
    }

    $('#l13').css({'left': mleft[13], 'width': mwidth[13], 'top': mtop[13]});

    mtop[14] = mtop[13];
    mwidth[14] = (((windowWidth * 5) / 100));
    mleft[14] = mleft[11] - mwidth[14];
    $('#l14').css({'left': mleft[14], 'width': mwidth[14], 'top': mtop[14]});

    pos1 = $('#for_line_15').offset();
    mtop[15] = mtop[13];
    mheight[15] = pos1.top - mtop[15] + (((windowWidth * 1) / 100));
    mleft[15] = mleft[11];

    if (windowWidth < 600) {
        mleft[15] = mleft[13];
        ppp = $('.b-sect.s4').offset();
        mheight[15] = ppp.top - mtop[13];
    }

    $('#l15').css({'left': mleft[15], 'height': mheight[15], 'top': mtop[15]});
    $('#l16').css({'left': mleft[15], 'width': 200, 'top': mtop[15] + mheight[15]});

    width1 = $('#for_line_15').width();
    mtop[17] = mtop[15] + mheight[15] + (((windowWidth * 5) / 100));
    mleft[17] = width1 + pos1.left - (((windowWidth * 5) / 100));
    mwidth[17] = mleft[15] - mleft[17] - (((windowWidth * 5) / 100));

    if (windowWidth < 1400) {
        mleft[17] = width1 + pos1.left;
        mwidth[17] = mleft[15] - mleft[17];
    }
    if (windowWidth < 600) {
        ppp = $('#meeting-rooms').offset();
        mleft[17] = 0;
        mwidth[17] = windowWidth * 0.1;
        mtop[17] = ppp.top - 20;
    }
    $('#l17').css({'left': mleft[17], 'width': mwidth[17], 'top': mtop[17]});

    pos1 = $('#for_line_18').offset();
    mleft[18] = mleft[17] + mwidth[17];
    mtop[18] = mtop[17];
    mheight[18] = pos1.top - mtop[18] + 50;

    if (windowWidth < 600) {
        pos1 = $('#for_line_15').offset();
        mheight[18] = pos1.top - mtop[18] - 20;
    }

    $('#l18').css({'left': mleft[18], 'height': mheight[18], 'top': mtop[18]});

    pos2 = $('#slider8').offset();
    height1 = $('#slider8').height();
    height2 = $('#for_line_18').height();
    mleft[19] = mleft[18] - (((windowWidth * 7) / 100));
    mtop[19] = pos1.top + height2 - 50;
    mheight[19] = pos2.top + height1 - mtop[19] + (((windowWidth * 7) / 100));
    if (windowWidth < 1400) {
        mtop[19] = pos2.top + height1 - 50;
        mleft[19] = mleft[18] - (((windowWidth * 20) / 100));
        pos4 = $('#for_line_21 p').offset()
        height4 = $('#for_line_21').height();

        mheight[19] = (pos4.top - pos2.top - height1) / 2 + 50;

    }
    $('#l19').css({'left': mleft[19], 'height': mheight[19], 'top': mtop[19]});

    mtop[20] = mtop[19] + mheight[19];
    mwidth[20] = windowWidth - mleft[19] + (((windowWidth * 25) / 100));

    if (windowWidth < 1400) {
        mwidth[20] = windowWidth - mleft[19] + (((windowWidth * 16) / 100));
    }
    mleft[20] = mleft[19] - mwidth[20] + Lh;
    $('#l20').css({'left': mleft[20], 'width': mwidth[20], 'top': mtop[20]});

    pos1 = $('#for_line_21').offset();
    mleft[21] = mleft[20];
    mtop[21] = mtop[20];
    mheight[21] = pos1.top - mtop[21] - (((windowWidth * 5) / 100));
    if (windowWidth < 1400) {
        mheight[21] = pos4.top - mtop[21] + height4 + (((windowWidth * 5) / 100));
    }
    $('#l21').css({'left': mleft[21], 'height': mheight[21], 'top': mtop[21]});

    mtop[22] = mtop[21] + mheight[21] - 2;
    mwidth[22] = mleft[21] - (((windowWidth * 10) / 100));
    mleft[22] = mleft[21] - mwidth[22] + Lh;
    if (windowWidth < 1400) {
        mleft[22] = (((windowWidth * 4) / 100));
        mwidth[22] = mleft[21] - mleft[22] + Lh;
    }

    if (windowWidth < 1100) {
        mleft[22] = (((windowWidth * 2) / 100));
        mwidth[22] = mleft[21] - mleft[22] + Lh;
    }

    if (windowWidth < 600) {
        pos1 = $('#for_line_15').offset();
        pos122 = $('#for_line_15').width();
        mtop[22] = mtop[18] + mheight[18] + 50;
        mleft[22] = windowWidth * 0.65;
        mwidth[22] = windowWidth * 0.31;
    }

    $('#l22').css({'left': mleft[22], 'width': mwidth[22], 'top': mtop[22]});

    pos1 = $('#for_line_23').offset();
    mleft[23] = mleft[22];
    mtop[23] = mtop[22];
    mheight[23] = pos1.top - mtop[22] + (((windowWidth * 4) / 100));
    if (windowWidth < 600) {
        mleft[23] = mleft[22] + mwidth[22];
        mtop[23] = mtop[22];
        mheight[23] = windowWidth * 0.4;
    }

    $('#l23').css({'left': mleft[23], 'height': mheight[23], 'top': mtop[23]});

    mtop[24] = mtop[23] + mheight[23];
    mleft[24] = mleft[23];
    mwidth[24] = pos1.left - mleft[24] - (((windowWidth * 5) / 100));
    $('#l24').css({'left': mleft[24], 'width': mwidth[24], 'top': mtop[24]});


    pos4 = $('#for_line_23').offset();
    width4 = $('#for_line_23').width();
    mtop[25] = mtop[24];
    mleft[25] = pos4.left + width4 - 20;
    mwidth[25] = (windowWidth - mleft[25]) / 2;
    $('#l25').css({'left': mleft[25], 'width': mwidth[25], 'top': mtop[25]});

    pos1 = $('#slider4 .slider3_item.slick-active').offset();
    height1 = $('#slider4 .slider3_item.slick-active').height();
    mleft[26] = mleft[25] + mwidth[25];
    mtop[26] = mtop[25];
    mheight[26] = pos1.top + height1 - mtop[26] - (((windowWidth * 4.5) / 100));

    if (windowWidth < 600) {

        pos1 = $('#conference-rooms').offset();
        mleft[26] = windowWidth / 2;
        mtop[26] = pos1.top - 64;
        mheight[26] = windowWidth * 0.3;
    }
    $('#l26').css({'left': mleft[26], 'height': mheight[26], 'top': mtop[26]});

    mtop[27] = mtop[26] + mheight[26];
    mwidth[27] = (((windowWidth * 71.5) / 100));
    mleft[27] = mleft[26] - mwidth[27] + Lh;

    if (windowWidth < 600) {
        mwidth[27] = windowWidth * 0.35;
        mleft[27] = mleft[26] - mwidth[27] + Lh;
    }

    $('#l27').css({'left': mleft[27], 'width': mwidth[27], 'top': mtop[27]});

    pos1 = $('#for_line28').offset();
    mleft[28] = mleft[27];
    mtop[28] = mtop[27];
    mheight[28] = pos1.top - mtop[27] + (((windowWidth * 5) / 100));
    if (windowWidth < 600) {
        mheight[28] = pos1.top - mtop[27] - windowWidth * 0.05;
    }

    $('#l28').css({'left': mleft[28], 'height': mheight[28], 'top': mtop[28]});

    mtop[29] = mtop[28] + mheight[28];
    mwidth[29] = pos1.left - mleft[28] - (((windowWidth * 5) / 100));
    mleft[29] = mleft[28];
    if (windowWidth < 600) {
        mwidth[29] = windowWidth * 0.77;
    }

    $('#l29').css({'left': mleft[29], 'width': mwidth[29], 'top': mtop[29]});


    pos1 = $('#for_line_30').offset();
    mtop[30] = pos1.top + (((windowWidth * 3) / 100));
    mwidth[30] = (((windowWidth * 5.5) / 100));
    mleft[30] = mleft[15] - mwidth[30];
    if (windowWidth < 800) {
        mleft[30] = mleft[15] - mwidth[30] + 10;
    }

    if (windowWidth < 600) {
        mtop[30] = mtop[29];
        mwidth[30] = 0;
        mleft[30] = mleft[29] + mwidth[29];
    }

    $('#l30').css({'left': mleft[30], 'width': mwidth[30], 'top': mtop[30]});


    pos1 = $('#events').offset();
    height1 = $('#events').height();
    mleft[31] = mleft[30] + mwidth[30];
    mtop[31] = mtop[30];
    mheight[31] = pos1.top - mtop[31] - (((windowWidth * 5) / 100));

    if (windowWidth < 600) {

        pos1 = $('.b-sect.s8').offset();
        mheight[31] = pos1.top - mtop[31] + windowWidth * 0.2;
    }

    $('#l31').css({'left': mleft[31], 'height': mheight[31], 'top': mtop[31]});

    mtop[32] = mtop[31] + mheight[31];
    mwidth[32] = (((windowWidth * 59) / 100));
    mleft[32] = mleft[31] - mwidth[32] + Lh;

    if (windowWidth < 600) {
        mleft[32] = mleft[31];
        mwidth[32] = windowWidth * .5;
    }

    $('#l32').css({'left': mleft[32], 'width': mwidth[32], 'top': mtop[32]});

    pos1 = $('#for_line_33').offset();
    height1 = $('#for_line_33').height();
    mleft[33] = mleft[32];
    mtop[33] = mtop[32];
    mheight[33] = pos1.top - mtop[33] - (((windowWidth * 2) / 100));
    $('#l33').css({'left': mleft[33], 'height': mheight[33], 'top': mtop[33]});

    if (windowWidth < 600) {
        pos2 = $('#events').offset();
        mtop[46] = pos2.top - windowWidth * 0.1;
        mleft[46] = 0;
        mwidth[46] = windowWidth * 0.09;
        $('#l46').css({'left': mleft[46], 'width': mwidth[46], 'top': mtop[46]});

        mtop[47] = mtop[46];
        mleft[47] = mwidth[46];
        mheight[47] = windowWidth * 0.4;
        $('#l47').css({'left': mleft[47], 'height': mheight[47], 'top': mtop[47]});
    }


    pos2 = $('#slider5 .slider5_item.slick-active a-event').offset();
    height2 = $('#slider5 .slider5_item.slick-active a-event').height();
    mtop[34] = pos1.top + height1 - 40;
    mleft[34] = windowWidth / 2 - (((windowWidth * 5) / 100));
    mwidth[34] = windowWidth / 2 - mleft[34] - 3;
    if (windowWidth < 600) {
        mtop[34] = pos1.top + height1;
    }

    $('#l34').css({'left': mleft[34], 'width': mwidth[34], 'top': mtop[34]});


    pos1 = $('#slider7').offset();
    mleft[35] = mleft[34] + mwidth[34] - 1;
    mtop[35] = mtop[34];
    mheight[35] = pos1.top - mtop[34] - (((windowWidth * 3.8) / 100));
    $('#l35').css({'left': mleft[35], 'height': mheight[35], 'top': mtop[35]});

    pos2 = $('#for_line_38').offset();
    height2 = $('#for_line_38').height();
    mleft[38] = mleft[35];
    mtop[38] = mtop[35];
    mheight[38] = pos2.top + height2 - mtop[35];

    if (windowWidth < 600) {
        mheight[38] = mheight[38] + 50;
    }

    $('#l38').css({'left': mleft[38], 'height': mheight[38], 'top': mtop[38]});

    mtop[36] = mtop[35] + mheight[35];
    mwidth[36] = mleft[35] - (((windowWidth * 7) / 100));
    if (windowWidth < 800) {
        mwidth[36] = mleft[35] - (((windowWidth * 2) / 100));
    }
    mleft[36] = mleft[35] - mwidth[36] + Lh + 1;
    if (windowWidth < 600) {
        mwidth[36] = windowWidth / 2 - windowWidth * 0.04;
        mleft[36] = mleft[35];
    }


    $('#l36').css({'left': mleft[36], 'width': mwidth[36], 'top': mtop[36]});

    pos2 = $('#slider6 .slider3_item.slick-active .slider3_item_in').offset();
    height2 = $('#slider6 .slider3_item.slick-active .slider3_item_in').height();
    mleft[37] = mleft[36];
    mtop[37] = mtop[36];
    mheight[37] = pos2.top + height2 * 0.8 - mtop[37];
    if (windowWidth < 600) {
        mleft[37] = mleft[36] + mwidth[36];
    }

    $('#l37').css({'left': mleft[37], 'height': mheight[37], 'top': mtop[37]});

    mtop[39] = mtop[37] + mheight[37];
    mleft[39] = mleft[37];
    mwidth[39] = pos2.left - mleft[37] - 4;
    $('#l39').css({'left': mleft[39], 'width': mwidth[39], 'top': mtop[39]});

    pos2 = $('#for_line_40').offset();
    height2 = $('#for_line_40').height();
    mleft[40] = mleft[39] + mwidth[39];
    mtop[40] = mtop[39];
    mheight[40] = pos2.top + height2 / 4 - mtop[39] - 10;
    $('#l40').css({'left': mleft[40], 'height': mheight[40], 'top': mtop[40]});

    mtop[41] = mtop[40] + mheight[40];
    mleft[41] = mleft[40];
    mwidth[41] = pos2.left - mleft[41] - (((windowWidth * 2) / 100));
    $('#l41').css({'left': mleft[41], 'width': mwidth[41], 'top': mtop[41]});

    pos1 = $('#for_line_42').offset();
    pos3 = $('.b-sect.s13').offset();
    height3 = $('.b-sect.s13').outerHeight();
    mleft[42] = pos1.left - Lh;
    mtop[42] = pos2.top + height2 + (((windowWidth * 7) / 100));
    mheight[42] = pos3.top + height3 - mtop[42];

    if (windowWidth < 600) {
        mleft[42] = windowWidth * 0.75;
        mtop[42] = pos2.top + height2 + (((windowWidth * 7) / 100));
        mheight[42] = pos3.top + height3 - mtop[42] - windowWidth * 0.1;

    }

    $('#l42').css({'left': mleft[42], 'height': mheight[42], 'top': mtop[42]});

    mtop[43] = mtop[42] + mheight[42] - Lh;
    mwidth[43] = mleft[42] - (((mleft[42] * 10) / 100));
    mleft[43] = mleft[42] - mwidth[43];

    if (windowWidth < 600) {
        mwidth[43] = mleft[42] - mleft[42] * 0.05;
        mleft[43] = mleft[42] - mwidth[43];
    }

    $('#l43').css({'left': mleft[43], 'width': mwidth[43], 'top': mtop[43]});


    pos1 = $('#mapID').offset();
    mleft[44] = mleft[43];
    mtop[44] = mtop[43];
    mheight[44] = pos1.top - mtop[44];
    $('#l44').css({'left': mleft[44], 'height': mheight[44], 'top': mtop[44]});
}


setTimeout(lineDraw, 1000);

/************************************************************
 *  LINES DRAW - END
 ************************************************************/