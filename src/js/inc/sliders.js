/************************************************************
*  SLIDERS - START
 ************************************************************/



$("#slider1").slick({
    infinite: true,
    initialSlide: $('#slider1').data('start_slide'),
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
});

$("#slider7").slick({
    infinite: false,
    initialSlide: $('#slider5').data('start_slide'),
    dots: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
});


$("#slider10").slick({
    swipe:false,
    touchMove: false,
    draggable: false,
    infinite: false,
    dots: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    cssEase: 'linear'
});


$("#slider2").slick({
    infinite: false,
    dots: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    fade: true
});

$('#slider2').on('afterChange', function (event, slick, currentSlide) {
    lineDraw(0, 0, true);

});

$("#slider_frm").slick({
    infinite: false,
    dots: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    fade: true
});


$('#frmpop_menu a').click(function () {
    var $this = $(this), idx = $this.parent().index();
    $('#frmpop_menu li').removeClass('active');
    $this.parent().addClass('active');
    $('#slider_frm').slick('slickGoTo', idx);
    return false;
});

$('#menumid a').click(function () {
    var $this = $(this), idx = $this.parent().index();
    $('#menumid li').removeClass('active');
    $this.parent().addClass('active');
    $('#slider2').slick('slickGoTo', idx);
    return false;
});

$('#menumid2 a').click(function () {
    var $this = $(this), idx = $this.parent().index();
    $('#menumid2 li').removeClass('active');
    $this.parent().addClass('active');

    $('#slider10').slick('slickGoTo', idx);
    if (idx == 0) {
        $('#slider3').slick('slickGoTo', 1);
    }
    $('#slider31').slick('slickGoTo', 1);
    $('#slider32').slick('slickGoTo', 1);
    $('#slider33').slick('slickGoTo', 1);

    return false;
});


$("#slider3,#slider4,#slider6,#slider8, #slider32").slick({
    infinite: false,
    initialSlide: 1,
    dots: true,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1
});


$("#slider31,#slider33").slick({
    initialSlide: 1,
    swipe:false,
    draggable: false,
    touchMove: false,
    infinite: false,
    dots: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,

});



$("#slider5").slick({
    infinite: false,
    arrows: true,
    slidesToShow: 1,
    initialSlide: $('#slider5').data('start_slide'),
    slidesToScroll: 1,
});

var currentSlide5 = $('#slider5').slick('slickCurrentSlide');


$('#slider5').find('.slider5_item[data-slick-index=' + (currentSlide5 - 1) + ']').addClass('bfr');
$('#slider5').find('.slider5_item[data-slick-index=' + (currentSlide5 + 1) + ']').addClass('aft');


$('.a-event').click(function () {
    var curSld = $(this).parent().parent().data('slick-index') * 1;

    currentSlide5 = $('#slider5').slick('slickCurrentSlide');
    if (currentSlide5 != curSld) {
        if (curSld - currentSlide5 > 1) {
            $('#slider5').slick('slickGoTo', curSld - 1);
            setTimeout(function () {
                $('#slider5').slick('slickGoTo', curSld);
            }, 510)

        } else if (curSld - currentSlide5 < (-1)) {
            $('#slider5').slick('slickGoTo', curSld + 1);
            setTimeout(function () {
                $('#slider5').slick('slickGoTo', curSld);
            }, 510)

        } else {
            $('#slider5').slick('slickGoTo', curSld);
        }
        dontSlick = false;
    }
    return false;
});


$('#slider5').on('beforeChange', function (event, slick, currentSlide, nextSlide) {

    if (!dontSlick) {
        $('#slider5 .slider5_item').removeClass('bfr').removeClass('aft');
        if (nextSlide > currentSlide) {
            $('#slider5').find('.slider5_item[data-slick-index=' + (currentSlide) + ']').addClass('bfr');
            $('#slider5').find('.slider5_item[data-slick-index=' + (currentSlide + 2) + ']').addClass('aft');
        } else {
            $('#slider5').find('.slider5_item[data-slick-index=' + (currentSlide) + ']').addClass('aft');
            $('#slider5').find('.slider5_item[data-slick-index=' + (currentSlide - 2) + ']').addClass('bfr');
        }
    }
});

$('#slider5').on('afterChange', function (event, slick, currentSlide) {
    $('#slider5 .slider5_item').removeClass('bfr').removeClass('aft');
    $('#slider5').find('.slider5_item[data-slick-index=' + (currentSlide - 1) + ']').addClass('bfr');
    $('#slider5').find('.slider5_item[data-slick-index=' + (currentSlide + 1) + ']').addClass('aft');
    $('#slider7').slick('slickGoTo', currentSlide);

});

/************************************************************
 *  SLIDERS - END
 ************************************************************/