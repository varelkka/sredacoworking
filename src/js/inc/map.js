/************************************************************
 *  MAP - START
 ************************************************************/


var stylesMAP = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"hue":"#ff0000"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#ff0000"}]}];
    var footer_map_coord = {lat: 55.033686, lng: 82.933034},
        footer_map_text = 'Привет!',
        footer_map_zoom = 14,
        footer_map_icon = 'svg/Coworking_logo.svg',
        mapIconWidth = 52,
        mapIconHeight = 44;


    function initMap() {
        var center_map = footer_map_coord;

        //center_map['lat'] = center_map['lat']-0.0020;

        var infowindow = new google.maps.InfoWindow({
            content: footer_map_text
        });

        if (windowWidth < 1200) {
            footer_map_zoom = footer_map_zoom - 1;
        }

        var map = new google.maps.Map(document.getElementById('mapID'), {
            zoom: footer_map_zoom,
            center: center_map,
            gestureHandling: 'greedy',
            draggable: true,
            //scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false
            // styles: [{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}]
        });
        map.setOptions({styles: stylesMAP});
        var marker = new google.maps.Marker({
            position: footer_map_coord,
            map: map,
            //title: " " + footer_map_title + " ",
            icon: {
                url: footer_map_icon,
                scaledSize: new google.maps.Size(mapIconWidth, mapIconHeight)
            }
        });
        /*
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });
                google.maps.event.addListener(marker, 'mouseover', function () {
                    infowindow.open(map, marker);
                });

                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
                */
        if (windowWidth < 450) {
            map.panBy(50, 0);
        } else if (windowWidth < 1200) {
            map.panBy(10, 0);

        } else {
            map.panBy(150, 0);
        }

        //map.panBx(-500,0);
    }


    initMap();

/************************************************************
 *  MAP - END
 ************************************************************/