/************************************************************
 *  POPUP - START
 ************************************************************/


$('.js-open').click(function () {
    var obj = $(this).attr('href');
    $('#shad').fadeIn();
    $(obj).fadeIn();

    if ($(this).hasClass('a-abon')) {
        var tit = $(this).find('.a-abon_tit').html();
        console.log(tit);
        $('#abonSelect').val(tit);
        $('#abonSelect').trigger('change');
    }

    if ($(this).hasClass('a-bron1')) {
        var tit = $(this).find('.a-bron_tit').html();
        console.log(tit);
        $('#bronSelect').val(tit);
        $('#bronSelect').trigger('change');
    }

    if ($(this).hasClass('a-bron2')) {
        var tit = $(this).find('.a-bron_tit').html();
        console.log(tit);
        $('#selectBronConf').val(tit);
        $('#selectBronConf').trigger('change');
    }


    if (obj == '#formEvent') {
        var tit = $(this).closest('.slider7_item').find('h4').html(),
            tit2 = $(this).closest('.slider7_item').find('.event_autor_name').html();
        tit2 = tit2.replace("<br>", " ");
        tit = tit.replace("<br>", " в ");
        $('#formEvent').find('.b-frmpop_tit').html(tit2);
        $('#formEvent_Confirm').find('.b-frmpop_txt span').html(tit);
    }


    if (obj == '#formLogin') {
        $("#slider_frm").slick('unslick');
        $("#slider_frm").slick({
            infinite: false,
            dots: false,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            fade: true
        });
    }
    $(obj).find('input:first').focus();

    return false;
});

$('#shad, .close-frm').click(function () {
    $('#shad, .b-frmpop').fadeOut();
    return false;
});

// Заглушка для обработки форм

$(".b-frmpop form").submit(function () {

    var form_id = $(this).attr('id'), form_parent_id = $(this).closest('.b-frmpop').attr('id'),
        formsArr = ['formLogin', 'formAbon', 'formBron', 'formBronConf', 'formEvent', 'formMessage'];

    if (formsArr.indexOf(form_parent_id) != -1) {
        $('#' + form_parent_id).fadeOut();
        $('#' + form_parent_id + '_Confirm').fadeIn();
        return false;
    }
    $('#shad,#' + form_parent_id).fadeOut();
    return false;
});


$('#abonSelect').select2({
    minimumResultsForSearch: Infinity
});
$('.js-select').select2({
    minimumResultsForSearch: Infinity
});

$('#city').select2({
    minimumResultsForSearch: Infinity,
    dropdownParent: $('#for_city')
});


$.datetimepicker.setLocale('ru');
$('#frmDate1,#frmDate2,#frmDate3').datetimepicker({
    //format: 'd.m.y',
    timepicker: false,
    todayButton: false,
    format: 'j F',
    formatDate: 'j F',
    validateOnBlur: false,
    lang: 'ru'
});

$('#frmTime1,#frmTime2,#frmTime3,#frmTime4').datetimepicker({
    //format: 'd.m.y',
    timepicker: true,
    datepicker: false,
    todayButton: false,
    format: 'H:i',
    lang: 'ru'
});


$('.a-fore-date').click(function () {
    var obj = $(this).attr('href');
    $(obj).datetimepicker('show');
    return false;
});


/************************************************************
 *  POPUP - END
 ************************************************************/