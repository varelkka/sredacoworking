var DEV = true;
var VER='?v=12';

if(DEV){
    VER='?v='+Math.floor(Date.now() / 1000);
}

var jsArr = [
        'js/jquery.min.js',
        'js/slick.js',
        'js/mask.js',
        'js/select2.full.min.js',
        'js/jquery.datetimepicker.full.min.js',
        'js/jquery.form-validator.min.js',
        'js/main.js'
    ],
    cssArr = [
        'css/bootstrap-grid-custom.min.css',
        'css/slick.css',
        'css/fonts.css',
        'css/select2.min.css',
        'css/jquery.datetimepicker.css',
        'css/main.css',
        'css/media.css'
    ];

var     gulp           = require('gulp'),
		browserSync    = require('browser-sync'),
		ftp            = require('vinyl-ftp'),
        sourcemaps = require('gulp-sourcemaps'),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify'),
		cleanCSS       = require('gulp-clean-css'),
		imagemin       = require('gulp-imagemin'),
		cache          = require('gulp-cache'),
		autoprefixer   = require('gulp-autoprefixer'),
        svgSprite = require('gulp-svg-sprite'),
        htmlmin = require('gulp-htmlmin'),
        rename = require('gulp-rename'),
        replace = require('gulp-replace-task'),
        include       = require("gulp-include");



    var sprtConfig = {
        mode: {
            css: {
                render: {
                    css: true
                }
            }
        }
    };


gulp.task('sprite', function () {
    return gulp.src('assets/templ/svg/*.svg')
        .pipe(svgSprite(sprtConfig))
        .pipe(gulp.dest('assets/templ/'));
})

gulp.task('browser-sync', function() {
    browserSync.init({    proxy: "sredawork"
    });
});

gulp.task('html', function()  {
    scrip = '<script src="js/all.min.js'+VER+'"></script>';
    style= '<link rel="stylesheet" href="css/all.min.css'+VER+'">';


    if(DEV){
        scrip = style = '';
        jsArr.forEach(function (el) {
            scrip+= '<script src="'+el+'"></script>\n'  ;
        })
        cssArr.forEach(function (el) {
            style+= '<link rel="stylesheet" href="'+el+'">\n'  ;
        })
        return gulp.src('src/index_.html')
            .pipe(replace({
                patterns: [{
                    match: '{{sripts}}',
                    replacement: scrip
                },
                    {
                        match: '{{styles}}',
                        replacement: style
                    }],
                usePrefix: false
            }))
            .pipe(include({
                extensions: "html",
                hardFail: true,

            }))
            .pipe(rename("index.html"))
            .pipe(gulp.dest("app"))
            .pipe(gulp.dest('src'));
    }

    return gulp.src('src/index_.html')
        .pipe(replace({
            patterns: [{
                match: '{{sripts}}',
                replacement: scrip
            },
                {
                    match: '{{styles}}',
                    replacement: style
                }],
            usePrefix: false
        }))
        .pipe(include({
            extensions: "html",
            hardFail: true,

        }))
        .pipe(htmlmin({collapseWhitespace: true, removeComments: true}))
        .pipe(rename("index.html"))
        .pipe(gulp.dest("app"))
        .pipe(gulp.dest('src'));
});

gulp.task('scripts', function() {
    var jsArrLoc=[];
        jsArr.forEach(function (el) {
            jsArrLoc.push( 'src/'+el);
    });

    gulp.src("src/js/main_.js")
        .pipe(include({
//            extensions: "js",
            hardFail: true,

        }))
        .pipe(rename("main.js"))
        .pipe(gulp.dest("src/js"));

    if(DEV){
        return gulp.src(jsArrLoc)
            .pipe(gulp.dest('app/js'));
    }

    return gulp.src(jsArrLoc)
        .pipe(sourcemaps.init())
        .pipe(concat('all.min.js'))
        .pipe(uglify({
            compress: {
                booleans: false
            }
        }))
        .pipe(sourcemaps.write('../js'))
        .pipe(gulp.dest('app/js'))
        .pipe(gulp.dest('src/js'));

});


gulp.task('css', function() {
    var cssArrLoc=[];
    cssArr.forEach(function (el) {
        cssArrLoc.push( 'src/'+el);
    });

    if(DEV){
        return gulp.src(cssArrLoc)
            .pipe(gulp.dest('app/css'));
    }

    return gulp.src(cssArrLoc)
        .pipe(sourcemaps.init())
        .pipe(concat('all.min.css'))
        //.pipe(autoprefixer(['last 15 versions']))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('../css'))
        .pipe(gulp.dest('src/css'))
        .pipe(gulp.dest('app/css'));

});


gulp.task('imagemin', function() {

    gulp.src('src/svg/**/*')
        .pipe(cache(imagemin()))
        .pipe(gulp.dest('app/svg'));

    return gulp.src('src/img/**/*')
        .pipe(cache(imagemin()))
        .pipe(gulp.dest('app/img'));

});


gulp.task('build', ['imagemin', 'css', 'scripts', 'html'], function() {

});



gulp.task('min', [ 'css', 'scripts'], function() {

});

gulp.task('watch', function() {
    gulp.watch('src/css/**/*', ['css']);
    gulp.watch(['src/js/**/*','!src/js/main.js'], ['scripts']);
    gulp.watch('src/img/**/*', ['imagemin']);
    gulp.watch('src/svg/**/*', ['svg']);
    gulp.watch(['src/*.html','!src/index.html'], ['html']);
});

/*
файлы main.js и index.html собирается автоматически
все правки нужно вносить в соответствуюцие файлы main_.js index_.html после чего пересобрать проект
 */

gulp.task('default', ['watch']);
